package contactos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.TreeMap;

public class Main {

	public static void main(String[] args) throws IOException {
		//Map utiliza dos valores, uno como clave y otro como valor
		Map<String, Integer> contactos = new TreeMap<>();
		//La siguiente l�nea sirve para procesar s�lo una l�nea
		BufferedReader linea = new BufferedReader (new InputStreamReader(System.in));
		boolean fin = false;
		do {
			System.out.print("> ");
			Scanner kb = new Scanner(linea.readLine());
			kb.useDelimiter(":");
			try {
				String comando = kb.next();
			if (comando.equals("buscar")) {
				try {
					String nombre = kb.next();
					Integer tlf = contactos.get(nombre);
					if (tlf != null) {
						System.out.println(nombre +" -> "+tlf);
					}
					else {
						System.out.println("Contacto no encontrado");
					}
				} catch (InputMismatchException e) {
					System.out.println("No has introducido un nombre");
				} catch (NoSuchElementException e) {
					System.out.println("No has introducido un nombre");
				}
			}
			else if (comando.equals("eliminar")) {
					try {
						String nombre = kb.next();
						Integer tlf = contactos.remove(nombre);
						if (tlf == null) {
							System.out.println("Contacto no encontrado");
						}
					} catch (InputMismatchException e) {
						System.out.println("No has introducido un nombre");
					} catch (NoSuchElementException e) {
						System.out.println("No has introducido un nombre");
					}
			}
			else if (comando.equals("salir")) {
				System.out.println("Saliendo del programa...");
				fin=true;
			}
			else {
				try {
					int telefono = kb.nextInt();
					Integer anterior = contactos.put(comando, telefono);
					if (anterior!=null) {
						System.out.printf("El tel�fono %d se ha cambiado por el tel�fono %d\n", anterior, telefono);
						
					}
				} catch (InputMismatchException e) {
					System.out.println("No has introducido un tel�fono");
				} catch (NoSuchElementException e) {
					System.out.println("No has introducido un tel�fono");
				}
			}
			} catch (NoSuchElementException e) {
				
			}
			kb.close();
		}
		while (!fin);
		System.out.println("Fin de programa");

	}

}
